![screenshot](/screenshot.png)

Black night theme

With remove Premium block at top-right

With removed block with languages and Reddit company links from bottom-left


For use with [Stylus](https://github.com/openstyles/stylus) for [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) or [Firefox](https://addons.mozilla.org/firefox/addon/styl-us).

Install theme from https://userstyles.world/style/14776/www-reddit-com-feb-2024
